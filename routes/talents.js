const express = require("express");
const {
  createTalent,
  getTalents,
  getDetailTalent,
  updateDataTalent,
  deleteTalent,
} = require("../controllers/talents");

const router = express.Router();

router.post("/", createTalent);
router.get("/", getTalents);
router.get("/:id", getDetailTalent);
router.put("/:id", updateDataTalent);
router.delete("/:id", deleteTalent);

module.exports = router;
