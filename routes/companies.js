const express = require("express");
const {
  createCompany,
  getCompanies,
  getDetailCompany,
  updateDataCompany,
  deleteCompany,
} = require("../controllers/companies");

const router = express.Router();

router.post("/", createCompany);
router.get("/", getCompanies);
router.get("/:id", getDetailCompany);
router.put("/:id", updateDataCompany);
router.delete("/:id", deleteCompany);

module.exports = router;
