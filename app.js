require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

// Config environment
const express = require("express"); // Import express
const fs = require("fs");
const path = require("path");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const rateLimit = require("express-rate-limit");
const hpp = require("hpp");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");

const app = express(); // Make express app

// CORS
app.use(cors());

// Sanitize data
app.use(mongoSanitize());

// Prevent XSS attact
app.use(xss());

// Rate limiting
const limiter = rateLimit({
  windowMs: 1000,
  max: 100,
});

app.use(limiter);

// Prevent http param pollution
app.use(hpp());

// Use helmet
app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

/* istanbul ignore next */
if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
  app.use(morgan("dev"));
} else {
  // create a write stream (in append mode)
  let accessLogStream = fs.createWriteStream(path.join(__dirname, "access.log"), {
    flags: "a",
  });

  // setup the logger
  app.use(morgan("combined", { stream: accessLogStream }));
}

/* Import routes */
const companies = require("./routes/companies");
// const pic = require("./routes/pic");
const talents = require("./routes/talents");
// const trackers = require("./routes/trackers");

/* Import errorHander */
const errorHandler = require("./middlewares/errorHandler");

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
  express.urlencoded({
    extended: true,
  })
);

/* Use the routes */
app.use("/companies", companies);
// app.use("/pic", pic);
app.use("/talents", talents);
// app.use("/trackers", trackers);

app.get("/", (req, res) => {
  /* istanbul ignore next */
  res.send("Success");
});

/* If route not found */
app.all("*", (req, res, next) => {
  try {
    /* istanbul ignore next */
    next({ message: "Endpoint not found", statusCode: 404 });
  } catch (error) {
    /* istanbul ignore next */
    next(error);
  }
});

/* Use error handler */
app.use(errorHandler);

/* Run the server */
/* istanbul ignore next */
if (process.env.NODE_ENV !== "test") {
  const server = app.listen(3000, () => console.log(`Server running on 3000`));
}

// Export app for testing
module.exports = app;
