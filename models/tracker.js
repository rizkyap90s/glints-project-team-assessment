const mongoose = require("mongoose");

const trackerSchema = new mongoose.Schema(
  {
    idTalent: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Talent",
    },
    idCompany: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Company",
    },
    idPic: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Pic",
    },
    status: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true, versionKey: false },
  }
);
module.exports = mongoose.model("Tracker", trackerSchema);
