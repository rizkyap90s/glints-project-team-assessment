require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment

const mongoose = require("mongoose");
mongoose
  .connect(process.env.MONGO_URI, {
    // useCreateIndex: true, // Enable unique
    useNewUrlParser: true, // Must be true
    useUnifiedTopology: true, // Must be true
    // useFindAndModify: false, // to use updateOne and updateMany
  })
  .then(() => console.log("MongoDB Connected"))
  /* istanbul ignore next */
  .catch((err) => console.log(err));

exports.Company = require("./company");
exports.Pic = require("./pic");
exports.Talent = require("./talent");
exports.Tracker = require("./tracker");
