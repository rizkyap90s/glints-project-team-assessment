const mongoose = require("mongoose");

const talentSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    stack: {
      type: String,
      required: true,
    },
    experience: {
      type: Number,
      required: true,
      default: 0,
      min: 0,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true, versionKey: false },
  }
);
module.exports = mongoose.model("Talent", talentSchema);
