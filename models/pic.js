const mongoose = require("mongoose");

const picSchema = new mongoose.Schema(
  {
    idTalent: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Talent",
    },
    idCompany: {
      type: mongoose.Types.ObjectId,
      required: true,
      ref: "Company",
    },
    name: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true, versionKey: false },
  }
);
module.exports = mongoose.model("Pic", picSchema);
