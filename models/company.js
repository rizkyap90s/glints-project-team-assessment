const mongoose = require("mongoose");

const companySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    stackNeeded: {
      type: String,
      required: true,
    },
    experienceNeeded: {
      type: Number,
      required: true,
      default: 0,
      min: 0,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true, versionKey: false },
  }
);
module.exports = mongoose.model("Company", companySchema);
