const { Company } = require("../models");

class Companies {
  async createCompany(req, res, next) {
    try {
      const data = await Company.create(req.body);
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async getCompanies(req, res, next) {
    try {
      const data = await Company.find();
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async getDetailCompany(req, res, next) {
    try {
      const data = await Company.findById(req.params.id);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateDataCompany(req, res, next) {
    try {
      const data = await Company.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async deleteCompany(req, res, next) {
    try {
      await Company.findOneAndDelete({ _id: req.params.id });
      res.status(201).json({ message: `company ${req.params.id} has deleted` });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Companies();
