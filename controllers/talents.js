const { Talent } = require("../models");

class Companies {
  async createTalent(req, res, next) {
    try {
      const data = await Talent.create(req.body);
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async getTalents(req, res, next) {
    try {
      const data = await Talent.find();
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async getDetailTalent(req, res, next) {
    try {
      const data = await Talent.findById(req.params.id);
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateDataTalent(req, res, next) {
    try {
      const data = await Talent.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true });
      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }
  async deleteTalent(req, res, next) {
    try {
      await Talent.findOneAndDelete({ _id: req.params.id });
      res.status(201).json({ message: `Talent ${req.params.id} has deleted` });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Companies();
